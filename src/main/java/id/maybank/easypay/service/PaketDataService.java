package id.maybank.easypay.service;

import java.util.List;
import java.util.Optional;

import id.maybank.easypay.model.PaketData;

public interface PaketDataService {
	
	public List<PaketData> getAll();
	public void save(PaketData pkt);
	Optional<PaketData> getPktById(Long id);
}
