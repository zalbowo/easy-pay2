package id.maybank.easypay.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.maybank.easypay.model.PaketData;
import id.maybank.easypay.repository.PaketDataRepo;

@Service
public class PaketDataServiceImpl implements PaketDataService{
	
	@Autowired
	private PaketDataRepo paketDataRepo;
	
	@Override
	public List<PaketData> getAll() {
		// TODO Auto-generated method stub
		return this.paketDataRepo.findAll();
	}

	@Override
	public void save(PaketData pkt) {
		// TODO Auto-generated method stub
		this.paketDataRepo.save(pkt);
	}

	@Override
	public Optional<PaketData> getPktById(Long id) {
		// TODO Auto-generated method stub
		return this.paketDataRepo.findById(id);
	}

}
