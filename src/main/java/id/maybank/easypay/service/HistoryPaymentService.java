package id.maybank.easypay.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.maybank.easypay.model.HistoryPayment;
import id.maybank.easypay.repository.HistoryPaymentRepo;

@Service
public class HistoryPaymentService {

	@Autowired
	HistoryPaymentRepo historyPaymentRepo;
	
	public List<HistoryPayment> getAllHistory(){
		return historyPaymentRepo.findAll();
	}
	
	public void addHistory(HistoryPayment historyPayment) {
		historyPaymentRepo.save(historyPayment);
	}
}
