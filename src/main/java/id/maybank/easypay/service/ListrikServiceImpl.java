package id.maybank.easypay.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.maybank.easypay.model.Listrik;
import id.maybank.easypay.repository.ListrikRepo;

@Transactional
@Service
public class ListrikServiceImpl implements ListrikService{

	@Autowired
	public ListrikRepo listrikRepo;
	
	@Override
	public void save(Listrik listrik) {
		// TODO Auto-generated method stub
		this.listrikRepo.save(listrik);
		
	}

	@Override
	public List<Listrik> getAllListrik() {
		// TODO Auto-generated method stub
		return this.listrikRepo.findAll();
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.listrikRepo.deleteById(id);
	}

}
