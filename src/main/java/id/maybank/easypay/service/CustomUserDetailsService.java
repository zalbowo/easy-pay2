package id.maybank.easypay.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import id.maybank.easypay.model.CustomUserDetails;
import id.maybank.easypay.model.User;
import id.maybank.easypay.repository.UserRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	UserRepository userRepository;
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		Optional<User> user = userRepository.findUserByEmail(email);
		user.orElseThrow(() -> new UsernameNotFoundException("User Not Found"));
		return user.map(CustomUserDetails::new).get();
	}


	public void adjustSaldo(String email, double totalAmount) {
		User user = userRepository.findOneByEmail(email);
		user.setSaldo(user.getSaldo()-totalAmount);
		userRepository.save(user);
	}
}
