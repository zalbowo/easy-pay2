package id.maybank.easypay.service;

import java.util.List;

import id.maybank.easypay.model.Listrik;

public interface ListrikService {
	
	public void save (Listrik listrik);
	
	public List<Listrik> getAllListrik ();
	
	public void delete (Long id);

}
