package id.maybank.easypay.service.pesawat;


import id.maybank.easypay.model.Pesawat;

import java.util.List;
import java.util.Optional;

public interface PesawatService {

    List<Pesawat> getAllPesawat();
    void savePesawat(Pesawat pesawat);
    List<Pesawat> getAllPesawatQuery(String asal, String tujuan, String kelas);
    Optional<Pesawat> getPesawatById(Long id);

}
