package id.maybank.easypay.service.pesawat;


import id.maybank.easypay.model.Pesawat;
import id.maybank.easypay.repository.PesawatRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class PesawatServiceImpl implements PesawatService{

    @Autowired
    private PesawatRepo pesawatRepo;

    @Override
    public List<Pesawat> getAllPesawat() {
        return this.pesawatRepo.findAll();
    }

    @Override
    public void savePesawat(Pesawat pesawat) {
        this.pesawatRepo.save(pesawat);
    }

    @Override
    public List<Pesawat> getAllPesawatQuery(String asal, String tujuan, String kelas) {
        return this.pesawatRepo.findAllPesawatByParams(asal, tujuan, kelas);
    }

    @Override
    public Optional<Pesawat> getPesawatById(Long id) {
        return this.pesawatRepo.findById(id);
    }
}