package id.maybank.easypay;

import id.maybank.easypay.model.Pesawat;
import id.maybank.easypay.repository.PesawatRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class MajorApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(MajorApplication.class, args);
	}

	@Autowired
	private PesawatRepo pesawatRepo;

	@Override
	public void run(String... args) throws Exception {

		Pesawat pesawat = new Pesawat();
		pesawat.setHarga(500000.0);
		pesawat.setKotaAsal("Jakarta");
		pesawat.setKotaTujuan("Denpasar");
		pesawat.setNamaMaskapai("Air Asia");
		pesawat.setTipeKelas("Ekonomi");
		pesawat.setWaktuTerbang("10.20");
		pesawat.setWaktuSampai("12.00");


		Pesawat pesawat1 = new Pesawat();
		pesawat1.setHarga(500000.0);
		pesawat1.setKotaAsal("Jakarta");
		pesawat1.setKotaTujuan("Medan");
		pesawat1.setNamaMaskapai("Citilink");
		pesawat1.setTipeKelas("Ekonomi");
		pesawat1.setWaktuTerbang("12.20");
		pesawat1.setWaktuSampai("14.00");

		Pesawat pesawat3 = new Pesawat();
		pesawat3.setHarga(500000.0);
		pesawat3.setKotaAsal("Jakarta");
		pesawat3.setKotaTujuan("Denpasar");
		pesawat3.setNamaMaskapai("Sriwijaya Air");
		pesawat3.setTipeKelas("Ekonomi");
		pesawat3.setWaktuTerbang("10.40");
		pesawat3.setWaktuSampai("12.10");

		Pesawat pesawat4 = new Pesawat();
		pesawat4.setHarga(500000.0);
		pesawat4.setKotaAsal("Jakarta");
		pesawat4.setKotaTujuan("Medan");
		pesawat4.setNamaMaskapai("Air Asia");
		pesawat4.setTipeKelas("Ekonomi");
		pesawat4.setWaktuTerbang("10.20");
		pesawat4.setWaktuSampai("12.00");

		List<Pesawat> listPesawat = new ArrayList<>();
		listPesawat.add(pesawat);
		listPesawat.add(pesawat1);
		listPesawat.add(pesawat3);
		listPesawat.add(pesawat4);

//		this.pesawatRepo.saveAll(listPesawat);

	}
}
