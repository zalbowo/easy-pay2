package id.maybank.easypay.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import id.maybank.easypay.global.Cart;
import id.maybank.easypay.model.CustomUserDetails;
import id.maybank.easypay.repository.ProductRepository;
import id.maybank.easypay.repository.UserRepository;

@Controller
public class PaymentController {
	
	
	@Autowired
	UserRepository repository;
	
	@Autowired
	ProductRepository productRepository;
	
	@GetMapping("/payNow")
	public String index(@AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
			
		
		
		
		System.out.println(customUserDetails.getUsername());
		return "redirect:/";
	}
}
