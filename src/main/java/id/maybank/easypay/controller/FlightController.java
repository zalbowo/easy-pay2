package id.maybank.easypay.controller;


import id.maybank.easypay.model.Pesawat;
import id.maybank.easypay.model.Tiket;
import id.maybank.easypay.repository.TiketRepo;
import id.maybank.easypay.service.pesawat.PesawatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/pesawat")
public class FlightController {

    @Autowired
    private PesawatService pesawatService;

    @Autowired
    private TiketRepo tiketRepo;

    @GetMapping
    public String index(Model model) {
        model.addAttribute("pesawat1", new Pesawat());
        model.addAttribute("pesawat2", new Pesawat());
        model.addAttribute("tiket", new Tiket());
        return "form-ticket";
    }


    @GetMapping("/tiket")
    public String listTiket(@RequestParam(value = "asal") String asal,
                            @RequestParam(value = "tujuan") String tujuan,
                            @RequestParam(value = "tglBerangkat") String date,
                            @RequestParam(value = "tipeKelas") String tipeKelas,
                            Tiket tiket,
                            Pesawat pesawat,
                            Model model) {


        List<Pesawat> queryPesawats = this.pesawatService.getAllPesawatQuery(asal, tujuan, tipeKelas);

        model.addAttribute("asal", asal);
        model.addAttribute("tujuan", tujuan);
        model.addAttribute("date", date);
        model.addAttribute("listPesawat", queryPesawats);
        model.addAttribute("tiket", new Tiket());
        model.addAttribute("pesawat", new Pesawat());

        return "list-tiket";
    }


    @PostMapping("/tiket/saveTicket")
    public String saveTiket(@ModelAttribute("tiket") Tiket tiket,
                            @ModelAttribute("pesawat") Pesawat pesawat,
                            Model model) {

        model.addAttribute("pesawat1", new Pesawat());
        model.addAttribute("pesawat2", new Pesawat());
        model.addAttribute("tiket", new Tiket());


        // get pesawat yg di pesan
        Pesawat queryPes = this.pesawatService.getPesawatById(pesawat.getId()).get();

        // generated kode tiket
        Double randomInteger = Math.random() * 10000000.0;

        // set tiket
        Tiket tiket1 = new Tiket();
        tiket1.setPesawat(queryPes);
        tiket1.setNamaPenumpang(tiket.getNamaPenumpang());
        tiket1.setTglBerangkat(tiket.getTglBerangkat());
        tiket1.setKodeTiket((int) Math.abs(randomInteger));

        this.tiketRepo.save(tiket1);

        return "redirect:/pesawat";

    }

}
