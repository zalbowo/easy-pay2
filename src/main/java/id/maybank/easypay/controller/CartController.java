package id.maybank.easypay.controller;

import id.maybank.easypay.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.fasterxml.jackson.annotation.JsonCreator.Mode;

import id.maybank.easypay.global.Cart;
import id.maybank.easypay.model.CustomUserDetails;
import id.maybank.easypay.model.HistoryPayment;
import id.maybank.easypay.model.Product;
import id.maybank.easypay.model.User;
import id.maybank.easypay.repository.HistoryPaymentRepo;
import id.maybank.easypay.repository.UserRepository;
import id.maybank.easypay.service.HistoryPaymentService;
import id.maybank.easypay.service.ProductService;

@Controller
public class CartController {

	@Autowired
	ProductService productService;
	@Autowired
	UserRepository userRepository;
	@Autowired
	HistoryPaymentService historyPaymentService;

	@Autowired
	CustomUserDetailsService customUserDetailsService;
	
	@GetMapping("/addToCart/{id}")
	public String addToCart(@PathVariable Long id) {
		Cart.cart.add(productService.getProductById(id).get());
		return "redirect:/shop";
	}
	
	@GetMapping("/cart")
	public String cart(Model model) {
		model.addAttribute("cartCount", Cart.cart.size());
		model.addAttribute("total", Cart.cart.stream().mapToDouble(Product::getPrice).sum());
		model.addAttribute("cart", Cart.cart);
		return "cart";
	}
	
	@GetMapping("/cart/removeItem/{index}") 	
	public String removeCart(@PathVariable int index) {
		Cart.cart.remove(index);
		return "redirect:/cart";
	}
	@GetMapping("/checkout")
	public String checkout(Model model) {
		model.addAttribute("history", new HistoryPayment());
		model.addAttribute("total", Cart.cart.stream().mapToDouble(Product::getPrice).sum());
		return "checkout";
	}
	
	@PostMapping("/payNow")
	public String payNow(@AuthenticationPrincipal CustomUserDetails customUserDetails, Model model, @ModelAttribute("history") HistoryPayment historyPayment) {
		model.addAttribute("email", customUserDetails.getEmail());
		model.addAttribute("total", Cart.cart.stream().mapToDouble(Product::getPrice).sum());
		historyPayment.setCarts(Cart.cart.stream().mapToDouble(Product::getPrice).sum());
		double totalAmount = Cart.cart.stream().mapToDouble(Product::getPrice).sum();
		customUserDetailsService.adjustSaldo(customUserDetails.getEmail(), totalAmount);
		this.historyPaymentService.addHistory(historyPayment);
		return "payNow";
	}
	
}
