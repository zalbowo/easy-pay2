package id.maybank.easypay.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import id.maybank.easypay.global.Cart;
import id.maybank.easypay.model.Listrik;
import id.maybank.easypay.service.CategoryService;
import id.maybank.easypay.service.ProductService;

@Controller
public class LandingPageController {

	@Autowired
	CategoryService categoryService;
	@Autowired
	ProductService productService;
	
	@GetMapping({"/", "/home"})
	public String index(Model model) {
		model.addAttribute("cartCount", Cart.cart.size());
		return "index";
	}
	
	@GetMapping("/shop")
	public String shop(Model model) {
		model.addAttribute("categories", categoryService.getAllCategory());
		model.addAttribute("products", productService.getAllProduct());
		model.addAttribute("cartCount", Cart.cart.size());
		return "shop";
	}
	
	@GetMapping("/shop/category/{id}")
	public String shopByCategory(Model model, @PathVariable int id) {
		model.addAttribute("categories", categoryService.getAllCategory());
		model.addAttribute("cartCount", Cart.cart.size());
		model.addAttribute("products", productService.getAllProductsByCategoryId(id));
		return "shop";
	}
	
	@GetMapping("/shop/viewproduct/{id}")
	public String viewProduct(Model model, @PathVariable Long id) {
		model.addAttribute("product", productService.getProductById(id).get());
		model.addAttribute("cartCount", Cart.cart.size());
		return "viewProduct";
	}
	
	@GetMapping("/topup")
	public String topup(Model model){
		model.addAttribute("listrik", new Listrik());
		return "topup";
	}
	
}
