package id.maybank.easypay.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import id.maybank.easypay.model.PaketData;
import id.maybank.easypay.service.PaketDataService;

@Controller
@RequestMapping("/paketdata")
public class PaketDataController {

	@Autowired
	private PaketDataService paketDataService;
	
	@GetMapping
	public String index(Model model) {
		java.util.List<PaketData> pktData = this.paketDataService.getAll();
		model.addAttribute("pktData", pktData);
		model.addAttribute("pktForm", new PaketData());
		return "paketdata";
	}
	@PostMapping("/save")
	public String save(@ModelAttribute("pktForm")PaketData paketData, Model model) {
		System.out.println(paketData);
		this.paketDataService.save(paketData);
		return "redirect:/paketdata";
	}
}
