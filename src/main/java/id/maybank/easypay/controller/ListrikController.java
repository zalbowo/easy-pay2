package id.maybank.easypay.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import id.maybank.easypay.model.Listrik;
import id.maybank.easypay.repository.ListrikRepo;
import id.maybank.easypay.service.ListrikService;

@Controller
@RequestMapping("/listrik")
public class ListrikController {
	
	@Autowired
	public ListrikService listrikService;
	
	@Autowired
	public ListrikRepo listrikRepo;
	
	@RequestMapping()
	public String view(Model model) {
		List<Listrik> listriks = this.listrikService.getAllListrik();
		model.addAttribute("listrik", new Listrik());
		model.addAttribute("listriks", listriks);
		
		return "listrikAdd";
	}
	
	@PostMapping("/save")
	public String saveListrik(@ModelAttribute("listrik") Listrik listrik) {
		
		this.listrikService.save(listrik);
		return "redirect:/admin/listrik";
	}

	@GetMapping("/delete")
	public String delete (@RequestParam("id") Long id, RedirectAttributes redirectAttributes) {
		
		this.listrikService.delete(id);
		return "redirect:/admin/listrik";
	}
	
	@PostMapping("/cektagihan")
	public String cekTagihan(@ModelAttribute("listrik") Listrik listrik, Model model){
		Listrik queryListrik = this.listrikRepo.findByNoMeter(listrik.getNoMeter());
		
		model.addAttribute("listrik", new Listrik());
		model.addAttribute("tagihan", queryListrik);
		
		return "topup";
	
	}
}
