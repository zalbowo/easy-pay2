package id.maybank.easypay.model;


import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Entity
public class Pesawat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private String namaMaskapai;
    @NotNull
    private String tipeKelas;
    @NotNull
    private String kotaAsal;
    @NotNull
    private String kotaTujuan;
    @NotNull
    private Double harga;
    private String waktuTerbang;
    private String waktuSampai;

    @OneToMany(mappedBy = "pesawat", fetch = FetchType.LAZY)
    private List<Tiket> listTiket;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaMaskapai() {
        return namaMaskapai;
    }

    public void setNamaMaskapai(String namaMaskapai) {
        this.namaMaskapai = namaMaskapai;
    }

    public String getTipeKelas() {
        return tipeKelas;
    }

    public void setTipeKelas(String tipeKelas) {
        this.tipeKelas = tipeKelas;
    }

    public String getKotaAsal() {
        return kotaAsal;
    }

    public void setKotaAsal(String kotaAsal) {
        this.kotaAsal = kotaAsal;
    }

    public String getKotaTujuan() {
        return kotaTujuan;
    }

    public void setKotaTujuan(String kotaTujuan) {
        this.kotaTujuan = kotaTujuan;
    }

    public Double getHarga() {
        return harga;
    }

    public void setHarga(Double harga) {
        this.harga = harga;
    }

    public String getWaktuTerbang() {
        return waktuTerbang;
    }

    public void setWaktuTerbang(String waktuTerbang) {
        this.waktuTerbang = waktuTerbang;
    }

    public String getWaktuSampai() {
        return waktuSampai;
    }

    public void setWaktuSampai(String waktuSampai) {
        this.waktuSampai = waktuSampai;
    }

    public List<Tiket> getListTiket() {
        return listTiket;
    }

    public void setListTiket(List<Tiket> listTiket) {
        this.listTiket = listTiket;
    }

}
