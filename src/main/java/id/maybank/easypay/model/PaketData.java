package id.maybank.easypay.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PaketData")
public class PaketData {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String nomor;
	private String provider;
	private String namaPaket;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNomor() {
		return nomor;
	}
	public void setNomor(String nomor) {
		this.nomor = nomor;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getNamaPaket() {
		return namaPaket;
	}
	public void setNamaPaket(String namaPaket) {
		this.namaPaket = namaPaket;
	}
	
	/*
	 * @Override public String toString() { return "PaketData [id=" + id +
	 * ", nomor=" + nomor + ", provider=" + provider + "]"; }
	 */
}
