package id.maybank.easypay.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import id.maybank.easypay.model.User;

public interface UserRepository extends JpaRepository<User, Integer>{

 	Optional<User> findUserByEmail(String email);

	User findOneByEmail(String email);
}
