package id.maybank.easypay.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.maybank.easypay.model.ListrikPayment;

public interface ListrikPaymentRepo extends JpaRepository<ListrikPayment, Long> {

}
