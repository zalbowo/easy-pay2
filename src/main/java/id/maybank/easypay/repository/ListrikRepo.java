package id.maybank.easypay.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.maybank.easypay.model.Listrik;


public interface ListrikRepo extends JpaRepository<Listrik, Long> {

	Listrik findByNoMeter(String noMeter);
	
}
