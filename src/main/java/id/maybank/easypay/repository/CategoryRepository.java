package id.maybank.easypay.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.maybank.easypay.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

	
}
