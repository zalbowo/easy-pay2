package id.maybank.easypay.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.maybank.easypay.model.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {

	
}
