package id.maybank.easypay.repository;

import id.maybank.easypay.model.Tiket;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TiketRepo extends JpaRepository<Tiket, Long> {
}
