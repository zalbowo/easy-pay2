package id.maybank.easypay.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.maybank.easypay.model.HistoryPayment;

public interface HistoryPaymentRepo extends JpaRepository<HistoryPayment, Long>{

}
