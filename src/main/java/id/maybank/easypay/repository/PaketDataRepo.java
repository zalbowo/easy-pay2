package id.maybank.easypay.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.maybank.easypay.model.PaketData;

public interface PaketDataRepo extends JpaRepository<PaketData, Long>{

}
