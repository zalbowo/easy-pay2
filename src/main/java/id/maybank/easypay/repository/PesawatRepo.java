package id.maybank.easypay.repository;

import id.maybank.easypay.model.Pesawat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PesawatRepo extends JpaRepository<Pesawat, Long> {

    @Query(value = "select * from pesawat where kota_asal = :asal and kota_tujuan = :tujuan and tipe_kelas = :kelas",
            nativeQuery = true)
    List<Pesawat> findAllPesawatByParams(String asal, String tujuan, String kelas);

}
